# mode-test

This project presents a simple way to get the mode from a number array passed as parameter to the function.

If the data set have two or more numbers with the same quantity, the first to appear on the array will be considered the mode.