from collections import Counter


def find_mode(temperatures):
    if len(temperatures) == 0:
        return "Erro"
    else:
        return Counter(temperatures).most_common(1)[0][0]
