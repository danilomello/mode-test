import unittest
from main.modatemperatura import find_mode


class TestStringMethods(unittest.TestCase):

    def test_deve_retornar_resultado(self):
        self.assertEqual(10, find_mode([10]))

    def test_resultado_deve_ser_um(self):
        self.assertEqual(1, find_mode([1, 1, 2]))

    def test_deve_retornar_resultado_quando_amodal(self):
        self.assertEqual(find_mode(1, [1, 1, 2, 2]))

    def test_deve_notificar_erro(self):
        self.assertEqual("Erro", find_mode([]))

    def test_resultado_deve_ser_negativo(self):
        self.assertEqual(find_mode(-1, [-1, 2, 2, -1, 1]))

    def test_resultado_deve_ser_zero(self):
        self.assertEqual(find_mode([0, 0, 0, 0, 0]), 0)


if __name__ == '__main__':
    unittest.main()